package dat251.factory.solution;

import java.util.Properties;
import java.io.FileInputStream;

public class CarFactory {
	
	private Properties props;
	private String packageName;
	
	public CarFactory() {
		try {
			props = new Properties();
			props.load(new FileInputStream("cars.properties"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		packageName = this.getClass().getPackage().getName();
	}
	
	public Car createFirstCar() {
		return createCar("firstCar");
		
	}
	
	public Car createSecondCar() {
		return createCar("secondCar");
	}
	
	private Car createCar(String propname) {
		try {
			return (Car) Class.forName(packageName + "." + props.getProperty(propname)).newInstance();
			
		} catch (Exception e) {
			return null;
		}
	}
}