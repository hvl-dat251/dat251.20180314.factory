package dat251.factory.solution;

public class CarRace {
	
	private Car firstCar;
	private Car secondCar;
		
	public CarRace() {
		CarFactory factory = new CarFactory();	
		firstCar = factory.createFirstCar();
		secondCar = factory.createSecondCar();
	}

	public void run() {
		System.out.println("First car in the race: " + firstCar);
		System.out.println("Second car in the race: " + secondCar);
	}
	
}